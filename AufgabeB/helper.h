#ifndef MY_HEADER_H
#define MY_HEADER_H
struct Mitglied {
	char nachname[40];
	char vorname[40];
	int rang;
};

struct Paare {
	char vorname_a[40];
	char nachname_a[40];
	char vorname_b[40];
	char nachname_b[40];
	int rankingSumme;
};

extern char vereina[12];
extern char vereinb[12];
extern struct Mitglied mitgliederA[30];
extern struct Mitglied mitgliederB[30];
extern struct Paare paarungen_final[30];
extern int anzahlA;
extern int anzahlB;

extern void mitgliederEinlesen (const char dateiname[], const int anzahl, struct Mitglied mitglieder[]);
extern int rangVgl(const void *a, const void *b);
extern void paarungenBilden (const int anzahlA, const struct Mitglied mitgliederA[], const int anzahlB, const struct Mitglied mitgliederB[], struct Paare paarungen[]);
extern void VerarbeiteArgumente(char ** argv);
#endif
