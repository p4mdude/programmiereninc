#include <stdio.h>
#include "helper.h"

int main(int argc, char *argv[])
{
    // Ueberpruefung der Anzahl der gegebenen Argumente.
    if (argc < 5)
    {
        printf("[Fehler]: Zu wenige Argumente bei Programmstart.");
        return 1;
    }
    VerarbeiteArgumente(argv);
    mitgliederEinlesen(vereina, anzahlA, mitgliederA);
    mitgliederEinlesen(vereinb, anzahlB, mitgliederB);
    paarungenBilden(anzahlA, mitgliederA, anzahlB, mitgliederB, paarungen_final);
    return 0;
}


