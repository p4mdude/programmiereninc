#include <errno.h>
#include <string.h> // genutzt fuer strcpy
#include <stdlib.h>
#include <stdio.h>
#include "helper.h"
#include <direct.h> // Genutzt um das CurrentWorkingDirectory herauszufinden
#define GetCurrentDir _getcwd

// Definition der im Header beschriebenen Prototypen
char vereina[12];
char vereinb[12];
struct Mitglied mitgliederA[30];
struct Mitglied mitgliederB[30];
struct Paare paarungen_final[30];
int anzahlA = 0;
int anzahlB = 0;

void mitgliederEinlesen (const char dateiname[], const int anzahl, struct Mitglied mitglieder[])
{
printf("\nVerarbeite Datei: %s\n\n", dateiname);
// Char-Arrays zur Unterscheidung zwischen wirklichen Daten und Delimitern
char data[100];
char separator[100];
// Zwei Zaehler werden benoetigt, counter fuer die kompletten Datensaetze, i fuer die einzelnen Elemente eines Datensatzes (Nachname, Vorname, Rang)
int i = 0;
int counter = 0;

FILE *file = fopen(dateiname, "r"); // Einlesen der spezifizierten Datei
// Falls ein Einlesen nicht moeglich ist, Rueckgabe des Fehlercodes.
if(!file) {
    printf("Fehler beim Oeffnen der Mitgliederdateien.\n");
    printf("Error %d \n", errno);
    if (file == NULL)
    {
        printf("Datei ist NULL");
    }
}

while(!feof(file)) { //solange die Datei nicht "zu Ende" ist, werden Daten eingelesen
    fscanf(file,"%[^ \n\t\r]s",data); // file wird bis zum ersten Auftreten eines "leeren" Zeichens durchsucht, der so entstehende stream wird in data gespeichert.
    i++;
    // Internes case-handling der eingehenden Zeilendaten. An Position 1 ist stets der Nachname, an 2 der Vorname, an 3 der Rang.
    if (i == 1)
    {
        strcpy(mitglieder[counter].nachname,data);
    }
    else if (i == 2)
    {

        strcpy(mitglieder[counter].vorname,data);
    }
    else if (i == 3)
    {
        mitglieder[counter].rang = atoi(data); //Umwandlung char zu int mittels atoi()
        printf("[DEBUG]: Mitglied %i: Nachname: %s Vorname: %s Rank: %i\n", counter+1, mitglieder[counter].nachname, mitglieder[counter].vorname, mitglieder[counter].rang);
        i = 0; //Reset des datensatzinternen Zaehlers
        counter++; //Erhoehen des Datensatzzaehlers
    }
    fscanf(file,"%[ \n\t\r]s",separator); //Entferne alle Zeichen, die "leer" sind (Zeilenumbrueche, Tabs, Leerzeichen, etc.) bis zum naechsten Auftreten eines anderen Zeichens
}
fclose(file); //schliesse Datei, falls das Ende erreich wurde
}

int rangVgl(const void *a, const void *b)
{
    //Integerverlgeich des kummulierten Ranges der beiden Mitglieder jeden Vereins, a,b sind die Integerwerte des "rankingSumme" des structs "Paare"
    int r1 = ((struct Paare *)a)->rankingSumme;
    int r2 = ((struct Paare *)b)->rankingSumme;
    return (r1 - r2);
    // ist das return value negativ, so hat Paar B ein hoeheres Ranking als Paar A, bei 0 sind sie gleich, bei positiven Werten ist Rank A > B
}

void paarungenBilden (const int anzahlA_, const struct Mitglied mitgliederA_[], const int anzahlB_, const struct Mitglied mitgliederB_[], struct Paare paarungen[])
{
    int i = 0;
    int limit = 0;
    // setze limit = der Anzahl Mitglieder des Vereins mit weniger Mitgliedern
    if (anzahlA_ > anzahlB_)
    {
        limit = anzahlB_;
    }
    else
    {
        limit = anzahlA_;
    }
    printf("\nAnzahl Mitglieder Verein A: %i\nAnzahl Mitglieder Verein B: %i\nMaximale Anzahlung moeglicher Paarungen: %i\n\n",anzahlA_,anzahlB_,limit);
    // Schreibe Daten der Mitglieder jedes Paares aus den getrennten Mitglied structs in das struct Paare, addiere die Rankings
    for (i = 0; i < limit; i++)
    {
        strcpy(paarungen[i].nachname_a, mitgliederA_[i].nachname);
        strcpy(paarungen[i].vorname_a, mitgliederA_[i].vorname);
        strcpy(paarungen[i].nachname_b, mitgliederB_[i].nachname);
        strcpy(paarungen[i].vorname_b, mitgliederB_[i].vorname);
        paarungen[i].rankingSumme = mitgliederA_[i].rang + mitgliederB_[i].rang;
    }
    qsort(paarungen, limit, sizeof(struct Paare), rangVgl); //Sortiere die Paare gemaess rangVgl, limit = Anzahl der zu sortierenden Elemente, sizeof(struct Paare) = Groesse der Elemente
    //Ausgabe Paare
    for (i = 0; i < limit; i++)
    {
        printf("Paarung %i: \t%s, %s vs. %s, %s. Ranking (kummuliert): %i\n", i+1, paarungen[i].nachname_a, paarungen[i].vorname_a, paarungen[i].nachname_b, paarungen[i].vorname_b, paarungen[i].rankingSumme);
    }
}

void VerarbeiteArgumente(char ** argv)
{
    strcpy(vereina,argv[1]);
    sscanf (argv[2],"%d",&anzahlA);
    strcpy(vereinb,argv[3]);
    sscanf (argv[4],"%d",&anzahlB);
}
