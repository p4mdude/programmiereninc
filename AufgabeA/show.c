#include <stdio.h>
#include <stdlib.h>
#include <math.h> // ben�tigt f�r die roundf Funktion

int roll_times;
void VisualisiereAnteil(float ProzentAnteil);
float BerechneAnteilZahlen(int Zahl, int AnzahlZahlen);

void show(int h[], int von, int bis)
{
    float ProzentAnteil = 0;
    if(sizeof(h) == 11) /* Limitierung Verteilungs-Array  */
    {
        for(int k = von; k < bis+1; k++)
        {
            ProzentAnteil = BerechneAnteilZahlen(h[k],roll_times);
            printf("%d :\t%d (%.2f %%)\t", k, h[k], ProzentAnteil); /* Ausgabe der Ergebnisse nach Wert aufsteigend */
            ProzentAnteil = roundf(ProzentAnteil * 100) / 100; /* K�rzen auf 3 Dezimalstellen und Runden */
            VisualisiereAnteil(ProzentAnteil);
            printf("\n");
        }
    }
    else if(sizeof(h) == 21 && von < 0, bis < 0) /* Limitierung Werte negativer Zahlen und 0 im Differenz-Array  */
    {
        von = von +10; /* Setze die intuitiven von und bis Werte auf die tats�chliche Array-Position */
        bis = bis +10;
        for (signed int d = von; d < bis+1; d++)
        {
            ProzentAnteil = BerechneAnteilZahlen(h[d+11],roll_times);
            printf("%d :\t%d (%.2f %%)\t\t", d-10, h[d+11],ProzentAnteil); /* Ausgabe der Ergebnisse nach Wert aufsteigend */
            ProzentAnteil = roundf(ProzentAnteil * 100) / 100; /* K�rzen auf 3 Dezimalstellen und Runden */
            VisualisiereAnteil(ProzentAnteil);
            printf("\n");
        }
    }
    else if (sizeof(h) == 21, von >= 0, bis >= 0) /* Limitierung Werte positiver Zahlen im Differenz-Array */
    {
        for (signed int d = von; d < bis+1; d++)
        {
            ProzentAnteil = BerechneAnteilZahlen(h[d],roll_times);
            printf("%d  :\t%d (%.2f %%)\t\t", d, h[d],ProzentAnteil); /* Ausgabe der Ergebnisse nach Wert aufsteigend */
            ProzentAnteil = roundf(ProzentAnteil * 100) / 100; /* K�rzen auf 3 Dezimalstellen und Runden */
            VisualisiereAnteil(ProzentAnteil);
            printf("\n");
        }
    }
    return;
}

float BerechneAnteilZahlen(int Zahl, int AnzahlZahlen) /* Berechnung der Anteile der einzelnen Werte im jeweiligen Array im Verh�ltnis zur Gesamtzahl */
{
    float AnteilZahl, ZahlTemp, AnzahlTemp;
    ZahlTemp = Zahl;            /* Konvertierung INT -> Float, sp�tere Vermeidung von int conversion overflow */
    AnzahlTemp = AnzahlZahlen;
    AnteilZahl = (ZahlTemp/AnzahlTemp)*100; /* Ausdruck als Anteil in Prozent */
    return AnteilZahl;
}

void VisualisiereAnteil(float ProzentAnteil)
{

    float AnzahlZeichen; /* interne Auslagerungvariable f�r case handling von kleinen / gro�en Anteilen*/
    int Begrenzung = 0; /* Kontrollvariable, ob Ausgabel�nge begrenz werden muss als Alternative zu bool */
    if (ProzentAnteil > 15.0) /* Begrenze Ausgabe auf 60 Zeichen, falls der Prozentanteil zu gro� wird; setze Kontrollvariable */
        {
            AnzahlZeichen = 60.0;
            Begrenzung = 1;
        }
    else /* Falls keine Begrenzung n�tig, kann der Prozentwert �bernommen werden */
        {
            AnzahlZeichen = ProzentAnteil;
        }
    for (float i = 0; i < AnzahlZeichen*4; i++) /* Je Prozent 4 * 'X' zur Balkenbildung */
        {
            printf("X");
            if (Begrenzung == 1 && i == 39) /* Falls Begrenzung notwendig ist und wir uns in der letzten Runde befinden setze ein Weiterf�hrungszeichen und beende die Visualisierung */
            {
                printf("->");
                return;
            }
        }
}
