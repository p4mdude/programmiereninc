#include <stdio.h>
#include <stdlib.h>
#include <time.h> //ben�tigt als Seed-Geber f�r den Zufallszahlengenerator
#include "show.h" //Ausgelagerte Ausgabefunktion

int roll_times = 0;

int main()
{
    srand(time(0)); //Systemzeit als Seed f�r den Zufallszahlengenerator
    int roll, i = 0;
    int VerteilungZahlen[11] = {0};
    int DifferenzZahlen[21] = {0};
    int ZwischenspeicherZahl = 0; /* Zur Differenzberechnung */
    float BerechneAnteilZahlen(int Zahl, int AnzahlZahlen);
    void VisualisiereAnteil(float ProzentAnteil);
    void show(int h[], int von, int bis);
    printf("Anzahl der Zufallszahlen: ");
    scanf("%i", &roll_times);
    printf("\n\nVerteilung der Werte 0-10 bei %d Zufallszahlen:\n\n", roll_times);
    for(i = 0; i < roll_times; i++)
    {
        int Differenz = 0;
        roll = rand() % 11; /* modulo 11 zur Ausgabe von Zahlen im Bereich 1-10 */
        VerteilungZahlen[roll]++; //Z�hler im Array f�r die generierte Zahl um 1 erh�hen
        if (i != 0) // Bei der Generierung der ersten Zahl kann noch kein Differenzvergleich stattfinden, daher diesen erst ab dem 2. Durchlauf aufrufen
        {
            Differenz = ZwischenspeicherZahl - roll; //Differenz ergibt sich aus dem zwischengespeicherten Wert der letzten Generierung und dem Wert der derzeitigen Iteration
            if (Differenz < 0)
            {
                Differenz = Differenz + 21; /* Zuweisen der designierten Array-Position */
            }
            DifferenzZahlen[Differenz]++;
        }
        ZwischenspeicherZahl = roll; /* Zwischenspeichern der generierten Zahl zur Differenzberechnung */
    }
    show(VerteilungZahlen, 0, 10);
    printf("\n\nVerteilung der Differenzen zwischen aufeinanderfolgenden Zahlen:\n\n");
    show(DifferenzZahlen, -10, -1);
    show(DifferenzZahlen, 0, 10);
    return 0;
}
